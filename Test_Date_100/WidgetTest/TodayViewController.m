//
//  TodayViewController.m
//  WidgetTest
//
//  Created by iOS_czw on 2017/1/5.
//  Copyright © 2017年 iOS_czw. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>

#import "WidgetView.h"


@interface TodayViewController () <NCWidgetProviding,WidgetDelegate>

@end

@implementation TodayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    WidgetView *headerView = [[WidgetView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 110)];
    headerView.delegate = self;
    [self.view addSubview: headerView];
    //self.headerView = headerView;
}

// 设置界面的高度
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.preferredContentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, 110);
}

- (void)WidgetView:(WidgetView *)view ClickSelectButton:(NSInteger)Tag{

    NSString *urlStr = [NSString stringWithFormat:@"medicalWdget://%li",Tag+1];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    [self.extensionContext openURL:url completionHandler:^(BOOL success) {
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.
    
    // If an error is encountered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData

    completionHandler(NCUpdateResultNewData);
}

@end
