//
//  WidgetView.m
//  Test_Date_100
//
//  Created by iOS_czw on 2017/1/5.
//  Copyright © 2017年 iOS_czw. All rights reserved.
//

#import "WidgetView.h"

#import "WGButton.h"

#define ios10_later [[UIDevice currentDevice].systemVersion doubleValue] >= 10.0

@implementation WidgetView

- (instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    if (self) {
        CGFloat magin = 10;
        CGFloat btnW = (frame.size.width - 6 * magin)/4;
        CGFloat btnH = frame.size.height - 2 * magin;
        NSArray *infoArr = [NSArray array];
        if (ios10_later) {
            
            infoArr = @[
                        @{@"imageName":@"diagnosis_wg",
                          @"title":@"消息中心"
                          },
                        @{@"imageName":@"season_wg",
                          @"title":@"养生咨询"
                          },
                        @{@"imageName":@"test_wg",
                          @"title":@"体能测试"
                          },
                        @{@"imageName":@"game_wg",
                          @"title":@"游戏中心"
                          },
                        ];
        }else{
            
            infoArr = @[
                        @{@"imageName":@"diagnosis_wg9",
                          @"title":@"消息中心"
                          },
                        @{@"imageName":@"season_wg9",
                          @"title":@"养生推荐"
                          },
                        @{@"imageName":@"test_wg9",
                          @"title":@"体能测试"
                          },
                        @{@"imageName":@"game_wg9",
                          @"title":@"游戏中心"
                          }
                        ];
        }
        for (int i = 0; i < 4; i ++) {
            
            WGButton *btn = [[WGButton alloc] initWithFrame:CGRectMake(magin * (i + 1) + btnW * i, magin , btnW , btnH)];
            [btn setImage:[UIImage imageNamed:infoArr[i][@"imageName"]] forState:UIControlStateNormal];
            [btn setTitle:infoArr[i][@"title"] forState:UIControlStateNormal];
            if (ios10_later) {
                
                [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            }else{
                
                [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            }
            [btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
            btn.tag = i;
            [self addSubview:btn];
        }
    }
    return self;
}

- (void)clickBtn:(UIButton *)sender{

    if ([self.delegate respondsToSelector:@selector(WidgetView:ClickSelectButton:)]) {
        [self.delegate WidgetView:self ClickSelectButton:sender.tag];
    }

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
