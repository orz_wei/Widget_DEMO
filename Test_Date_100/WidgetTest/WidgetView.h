//
//  WidgetView.h
//  Test_Date_100
//
//  Created by iOS_czw on 2017/1/5.
//  Copyright © 2017年 iOS_czw. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WidgetView;

@protocol WidgetDelegate <NSObject>

- (void)WidgetView:(WidgetView *)view ClickSelectButton:(NSInteger)Tag;

@end

@interface WidgetView : UIView

@property (nonatomic,assign) id<WidgetDelegate> delegate;

@end
