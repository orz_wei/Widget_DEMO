//
//  AppDelegate.m
//  Test_Date_100
//
//  Created by iOS_czw on 2017/1/2.
//  Copyright © 2017年 iOS_czw. All rights reserved.
//

#import "AppDelegate.h"

#import "SpTabbarViewController.h"

#import "PushViewController.h"

#import "BannerViewController.h"


@interface AppDelegate ()

@property (nonatomic,copy) NSString *host;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectRowNotification:) name:@"ExtenicationNotification" object:nil];
    
    self.window.rootViewController = [SpTabbarViewController new];
    
    return YES;
}

#pragma mark - didSelectRowAtIndexPath:
- (void)didSelectRowNotification:(NSNotification *)noti{

    UITabBarController *tabbar = (UITabBarController *)self.window.rootViewController;
    
    id show = noti.object;
    
    if ([show isKindOfClass:[NSString class]]) {
        NSInteger select = [show integerValue];
        
        if (select == 4) {
            
            tabbar.selectedIndex = 2;
            UINavigationController  *nvc = tabbar.selectedViewController;
            PushViewController *pushVC = [PushViewController new];
            pushVC.hidesBottomBarWhenPushed = YES;
            [nvc pushViewController:pushVC animated:YES];
            
        }else if(select == 1){
            
            tabbar.selectedIndex = 0;
            
            BannerViewController *banner = [BannerViewController new];
            UINavigationController  *nvc = tabbar.selectedViewController;
            banner.hidesBottomBarWhenPushed = YES;
            [nvc pushViewController:banner animated:YES];
            
        }else if (select == 2){
        
            tabbar.selectedIndex = 1;
            
        }else{
            tabbar.selectedIndex = 2;
        }
  
    }
    
}

// ios9 之前
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    NSLog(@"======= url ==== %@",url.host);

    if ([url.scheme isEqualToString:@"medicalWdget"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ExtenicationNotification" object:url];
        
    }
    return YES;
}

// ios9 之后
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options{

    NSString *msg = [NSString stringWithFormat:@"当前选择的是第%@个按钮",url.host];

    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"通过Widget " message:msg delegate:nil cancelButtonTitle:nil otherButtonTitles:@"qu xiao ", nil];
    [alertView show];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ExtenicationNotification" object:url.host];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
