//
//  main.m
//  Test_Date_100
//
//  Created by iOS_czw on 2017/1/2.
//  Copyright © 2017年 iOS_czw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
