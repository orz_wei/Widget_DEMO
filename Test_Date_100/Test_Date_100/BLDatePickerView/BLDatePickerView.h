//
//  BLAreaPickerView.h
//  AreaPicker
//
//  Created by boundlessocean on 2016/11/21.
//  Copyright © 2016年 ocean. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol BLDatePickerViewDelegate <NSObject>

@optional
// 滑动结束回调选择的天数
- (void)bl_selectedDateResultWithYear:(NSString *)year
                                month:(NSString *)month
                                  day:(NSString *)day;
@end

@interface BLDatePickerView : UIView

/** ---------------------- can adjustion property ----------------------*/

/** 选择器背景颜色 */
@property (nonatomic, strong)UIColor *pickViewBackgroundColor;


/** ---------------------- block & delegate ----------------------*/
/** 确定按钮点击 */
@property (nonatomic, copy) void(^sureButtonClickBlcok)(NSString *year, NSString *month, NSString *day);
/** 选择器代理 */
@property (nonatomic, weak) id<BLDatePickerViewDelegate> pickViewDelegate;



/** ---------------------- Action ----------------------*/

/** 设置默认日期 */
- (void)bl_setUpDefaultDateWithYear:(NSInteger)year
                              month:(NSInteger)month
                                day:(NSInteger)day;
@end
