//
//  ViewController.m
//  Test_Date_100
//
//  Created by iOS_czw on 2017/1/2.
//  Copyright © 2017年 iOS_czw. All rights reserved.
//

#import "ViewController.h"

#import "BLDatePickerView.h"

@interface ViewController ()<BLDatePickerViewDelegate>
{
    UILabel *_showLabel;
}
@property (nonatomic, strong) BLDatePickerView *datePickerView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"消息展示";
    
    self.view.backgroundColor = [UIColor grayColor];
    _showLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 200, 200, 30)];
    _showLabel.text = @"请选择日期";
    [self.view addSubview:_showLabel];
    
    [self.view addSubview:self.datePickerView];
    
}

#pragma mark - - lazy load
// 第一步
- (BLDatePickerView *)datePickerView{
    if (!_datePickerView) {
        _datePickerView = [[BLDatePickerView alloc] initWithFrame:CGRectMake(0, 100, 100, 100)];
        _datePickerView.pickViewDelegate = self;
        /**设置当前的时间 == 默认获取当前的时间=== */
        NSDate *currentDate = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"YYYY-MM-dd"];
        NSString *dateString = [dateFormatter stringFromDate:currentDate];
        dateString = @"1981-01-08";
        NSArray *currentDays = [dateString componentsSeparatedByString:@"-"];
        [_datePickerView bl_setUpDefaultDateWithYear:[currentDays[0]integerValue] month:[currentDays[1]integerValue] day:[currentDays[2]integerValue]];
        /** 可设置的属性 */
        _datePickerView.pickViewBackgroundColor = [UIColor clearColor];

        //        /** 设置背景透明度 0~1 */
        //        @property (nonatomic, assign)CGFloat backGAlpha;
        
    }
    return _datePickerView;
}

#pragma mark --- 选择的日期
- (void)bl_selectedDateResultWithYear:(NSString *)year month:(NSString *)month day:(NSString *)day{

    _showLabel.text = [NSString stringWithFormat:@"%@%@%@",year,month,day];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
