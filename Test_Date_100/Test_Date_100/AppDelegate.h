//
//  AppDelegate.h
//  Test_Date_100
//
//  Created by iOS_czw on 2017/1/2.
//  Copyright © 2017年 iOS_czw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

