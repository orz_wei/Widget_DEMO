//
//  SpTabbarViewController.m
//  Test_Date_100
//
//  Created by iOS_czw on 2017/1/5.
//  Copyright © 2017年 iOS_czw. All rights reserved.
//

#import "SpTabbarViewController.h"

#import "ViewController.h"
#import "HomeViewController.h"
#import "MySelfViewController.h"

@interface SpTabbarViewController ()

@end

@implementation SpTabbarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatViewController];
}

- (void)creatViewController{

    HomeViewController *homeVC = [HomeViewController new];
    UINavigationController *nav_home = [[UINavigationController alloc] initWithRootViewController:homeVC];
    nav_home.tabBarItem.title = @"Home";
    
    ViewController *view = [ViewController new];
    UINavigationController *nav_view = [[UINavigationController alloc] initWithRootViewController:view];
    nav_view.tabBarItem.title = @"Show";
    
    MySelfViewController *myVC = [MySelfViewController new];
    UINavigationController *nav_myVC = [[UINavigationController alloc] initWithRootViewController:myVC];
    nav_myVC.tabBarItem.title = @"Me";
    self.viewControllers = @[nav_home,nav_view,nav_myVC];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
